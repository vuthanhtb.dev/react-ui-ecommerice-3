import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';

import 'bootstrap/dist/css/bootstrap.css';
import 'remixicon/fonts/remixicon.css';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import 'index.css';
import 'styles/header.css';
import 'styles/home.css';
import 'styles/category.css';
import 'styles/slider.css';
import 'styles/all-foods.css';
import 'styles/pagination.css';
import 'styles/checkout.css';
import 'styles/cart-page.css';
import 'styles/cart-item.css';
import 'styles/shopping-cart.css';
import 'styles/hero-section.css';
import 'styles/common-section.css';
import 'styles/product-card.css';
import 'styles/product-details.css';
import 'styles/footer.css';

import App from './App';
import store from './store';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <BrowserRouter>
      <Provider store={store}>
        <App />
      </Provider>
    </BrowserRouter>
  </React.StrictMode>
);
