import React from 'react';
import { useSelector } from 'react-redux';
import Routers from 'routes';
import { CartComponent, FooterComponent, HeaderComponent } from 'components';

const App = () => {
  const showCart = useSelector((state) => state.cartUi.cartIsVisible);

  return (
    <div className="app">
      <HeaderComponent />
      {showCart && <CartComponent />}
      <Routers />
      <FooterComponent />
    </div>
  );
};

export default App;
