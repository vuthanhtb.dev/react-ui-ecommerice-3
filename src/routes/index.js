import { Routes, Route, Navigate } from 'react-router-dom';
import {
  AllFoodsPage,
  CartPage,
  CheckoutPage,
  ContactPage,
  FoodDetailPage,
  HomePage,
  LoginPage,
  RegisterPage
} from 'pages';

const Routers = () => {
  return (
    <Routes>
      <Route path="/" element={<Navigate to="/home" />} />
      <Route path="/home" element={<HomePage />} />
      <Route path="/foods" element={<AllFoodsPage />} />
      <Route path="/foods/:id" element={<FoodDetailPage />} />
      <Route path="/cart" element={<CartPage />} />
      <Route path="/checkout" element={<CheckoutPage />} />
      <Route path="/login" element={<LoginPage />} />
      <Route path="/register" element={<RegisterPage />} />
      <Route path="/contact" element={<ContactPage />} />
    </Routes>
  );
};

export default Routers;
