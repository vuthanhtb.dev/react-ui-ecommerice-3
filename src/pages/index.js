export { default as AllFoodsPage } from './all-foods';
export { default as CartPage } from './cart';
export { default as CheckoutPage } from './checkout';
export { default as ContactPage } from './contact';
export { default as FoodDetailPage } from './food-detail';
export { default as HomePage } from './home';
export { default as LoginPage } from './login';
export { default as RegisterPage } from './register';
