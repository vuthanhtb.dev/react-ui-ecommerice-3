import React, { useRef } from 'react';
import { Link } from 'react-router-dom';
import { Col, Container, Row } from 'reactstrap';
import { CommonSectionComponent, HelmetComponent } from 'components';

const Login = () => {

  const loginNameRef = useRef();
  const loginPasswordRef = useRef();

  const submitHandler = (event) => {
    event.preventDefault();
  };

  return (
    <HelmetComponent title="Login">
      <CommonSectionComponent title="Login" />
      <section>
        <Container>
          <Row>
            <Col lg="6" md="6" sm="12" className="m-auto text-center">
              <form className="form mb-5" onSubmit={submitHandler}>
                <div className="form__group">
                  <input
                    type="email"
                    placeholder="Email"
                    required
                    ref={loginNameRef}
                  />
                </div>
                <div className="form__group">
                  <input
                    type="password"
                    placeholder="Password"
                    required
                    ref={loginPasswordRef}
                  />
                </div>
                <button type="submit" className="addTOCart__btn">
                  Login
                </button>
              </form>
              <Link to="/register">
                Don't have an account? Create an account
              </Link>
            </Col>
          </Row>
        </Container>
      </section>
    </HelmetComponent>
  );
};

export default Login;
